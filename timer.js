"Use strict"

// fonction demarrerTimer
function demarrerTimer() {
  valeurTimer = 61;
  actualiserTimer()
}

function actualiserTimer() {
  if (valeurTimer > 0) {
    valeurTimer = valeurTimer - 1;
    document.getElementById("affichageTimer")
    .innerHTML = valeurTimer;
    if(valeurTimer > 0){
      monTimer = setTimeout('actualiserTimer()', 1000);
    }
  }
}

// affichage de la valeur actualisée du timer

document.getElementById('affichageTimer').innerHTML = valeurTimer;

// fonction arreterTimer non utilisé pour l'instant
// function arreterTimer() {
//   clearTimeout(monTimer);
// }

